<?php

/**
 * @file
 * owlsync_fields.module
 */

/**
 * Implements hook_theme().
 */
function owlsync_theme() {
  return array(
    'owlsync' => array(
      'variables' => array('items' => array(), 'settings' => array()),
      'file' => 'theme/owlsync.theme.inc',
    ),
    'owlsync_wrapper' => array(
      'variables' => array('output' => array()),
      'file' => 'theme/owlsync.theme.inc',
    ),
  );
}

/**
 * Implements hook_field_formatter_info().
 */
function owlsync_field_formatter_info() {
  return array(
    'owlsync_formatter' => array(
      'label' => t('Owl Sync'),
      'field types' => array('image'),
      'settings' => array(
        'settings_group' => 'default',
        'image_style' => 'large',
        'settings_group_preview' => 'default',
        'image_style_preview' => 'thumbnail',
      ),
    ),
  );
}


/**
 * Implements hook_field_formatter_settings_form().
 */
function owlsync_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  $element = array();
  $element['settings_group'] = array(
    '#type' => 'select',
    '#title' => t('Settings Group'),
    '#description' => t('Select the settings group to apply to the main
    slider.'),
    '#default_value' => $settings['settings_group'],
    '#options' => owlcarousel_instance_callback_list(),
  );

  $element['image_style'] = array(
    '#type' => 'select',
    '#title' => t('Image Style'),
    '#description' => t('Apply image style to the main slider.'),
    '#default_value' => $settings['image_style'],
    '#options' => image_style_options(),
  );

  $element['settings_group_preview'] = array(
    '#type' => 'select',
    '#title' => t('Preview Settings Group'),
    '#description' => t('Select the settings group to apply to the preview
    slider.'),
    '#default_value' => $settings['settings_group_preview'],
    '#options' => owlcarousel_instance_callback_list(),
  );

  $element['image_style_preview'] = array(
    '#type' => 'select',
    '#title' => t('Preview image Style'),
    '#description' => t('Apply image style to preview items.'),
    '#default_value' => $settings['image_style_preview'],
    '#options' => image_style_options(),
  );

  return $element;
}


/**
 * Implements hook_field_formatter_settings_summary().
 */
function owlsync_field_formatter_settings_summary($field, $instance, $view_mode) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];
  $message = '@settings with image style @style applied to carousel instance.
  <br> @settings_preview with image style @style_preview applied to carousel
  instance.';

  $summary = t($message, array(
    '@settings' => $settings['settings_group'],
    '@style' => $settings['image_style'],
    '@settings_preview' => $settings['settings_group_preview'],
    '@style_preview' => $settings['image_style_preview'],
  ));

  return $summary;
}

/**
 * Implements hook_field_formatter_view().
 */
function owlsync_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {

  $element = array();
  $settings = $display['settings'];

  // Format carousel settings.
  $items_group = FALSE;
  $settings_group = array(
    'instance' => $settings['settings_group'],
    'id' => 'owlcarousel-fields-' . $instance['id'],
  );
  // Format carousel settings.
  $items_group_preview = FALSE;
  $settings_group_preview = array(
    'instance' => $settings['settings_group_preview'],
    'id' => 'owlsync-fields-' . $instance['id'],
  );

  // Format carousel items.
  foreach ($items as $item) {
    $vars = array(
      'path' => $item['uri'],
      'width' => $item['width'],
      'height' => $item['height'],
      'alt' => $item['alt'],
      'title' => $item['title'],
    );

    if (empty($settings['image_style'])) {
      $items_group[]['row'] = theme('image', $vars);
      $items_group_preview[]['row'] = theme('image', $vars);
    }
    else {
      $vars += array('style_name' => $settings['image_style']);
      $items_group[]['row'] = theme('image_style', $vars);
      $vars['style_name'] = $settings['image_style_preview'];
      $items_group_preview[]['row'] = theme('image_style', $vars);
    }
  }

  // Only render if we have items.
  if ($items_group) {
    $element[] = array(
      '#theme' => 'owlcarousel',
      '#items' => $items_group,
      '#settings' => $settings_group
    );
  }
  // Only render if we have items.
  if ($items_group_preview) {
    $element[] = array(
      '#theme' => 'owlsync',
      '#items' => $items_group_preview,
      '#settings' => $settings_group_preview,
    );
  }

  $element['#attached']['library'][] = array('owlsync', 'owl-sync');

  return $element;
}

/**
 * Implements hook_library().
 */
function owlsync_library() {
  // Libraries.
  $libraries['owl-sync'] = array(
    'title' => 'owlsync',
    'version' => '0.1',
    'js' => array(
      drupal_get_path('module', 'owlsync') . '/js/owl.sync.js' => array(
        'scope' => 'footer'
      ),
    ),
  );

  return $libraries;
}
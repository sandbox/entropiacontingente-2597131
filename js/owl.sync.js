/**
 * @file
 * Initiate Owl Carousel sync.
 */

(function ($, Drupal) {
    "use strict";

    Drupal.behaviors.owlsync = {
        attach: function (context, settings) {

            function center(number, el) {
                var sync2visible = el.data("owlCarousel").owl.visibleItems,
                    num = number,
                    found = false;
                for (var i in sync2visible) {
                    if (num === sync2visible[i]) {
                        found = true;
                    }
                }

                if (found === false) {
                    if (num > sync2visible[sync2visible.length - 1]) {
                        el.trigger("owl.goTo", num - sync2visible.length + 2)
                    } else {
                        if (num - 1 === -1) {
                            num = 0;
                        }
                        el.trigger("owl.goTo", num);
                    }
                } else if (num === sync2visible[sync2visible.length - 1]) {
                    el.trigger("owl.goTo", sync2visible[1])
                } else if (num === sync2visible[0]) {
                    el.trigger("owl.goTo", num - 1)
                }
            }

            function syncPosition(el) {
                var current = this.currentItem;
                el
                    .find(".owl-item")
                    .removeClass("is--synced")
                    .eq(current)
                    .addClass("is--synced");
                if (el.data("owlCarousel") !== undefined) {
                    center(current, el)
                }
            }

            for (var carousel in settings.owlcarousel) {
                // sync instance.

                if (carousel.indexOf('owlsync') > -1) {
                    var sync = $('.' + carousel),
                        car = carousel.replace('owlsync', 'owlcarousel'),
                        owl = $('.' + car);

                    // Attach instance settings.
                    settings.owlcarousel[car].settings.afterAction = function (el) {
                        syncPosition.call(this, sync, el);
                    };
                    settings.owlcarousel[carousel].settings.afterAction = function (el) {
                        el.find(".owl-item").eq(0).addClass("is--synced");
                    };

                    sync.on("click", ".owl-item", function (e) {
                        e.preventDefault();
                        var number = $(this).data("owlItem");
                        owl.trigger("owl.goTo", number);
                    });

                }
            }
        }
    };

}(jQuery, Drupal));

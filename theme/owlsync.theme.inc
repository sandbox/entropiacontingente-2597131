<?php

/**
 * @file
 * owlsync.theme.inc
 *
 * Implements theme functions for Owl Carousel.
 */

/**
 * Template process carousel theme.
 */
function template_process_owlsync(&$vars) {
  $instance_id = $vars['settings']['instance'];
  $vars['settings']['attributes']['class'][] = $vars['settings']['id'];
  $vars['settings']['attributes']['class'][] = 'owlsync';
}

/**
 * Theme declaration for Owl Carousel Sync wrapper.
 */
function theme_owlsync_wrapper($variables) {
  $attributes = $variables['output']['#settings']['attributes'];
  return '<div' . drupal_attributes($attributes) . '>' .
  drupal_render($variables['output']) . '</div>';
}

/**
 * Theme declaration for Owl Carousel.
 */
function theme_owlsync($variables) {
  $instance = $variables['settings']['id'];
  $settings = $variables['settings'];
  $items = $variables['items'];

  $output = array(
    '#output' => array(
      'settings' => $settings,
      'items' => $items
    ),
    '#pre_render' => array('owlsync_pre_render_cache'),
  );

  return drupal_render($output);
}

/**
 * Construct element for render.
 */
function owlsync_pre_render_cache($element) {
  $settings = $element['#output']['settings']['instance'];
  $instance = $element['#output']['settings']['id'];

  $output = array(
    '#theme' => 'owlcarousel_list',
    '#items' => $element['#output']['items'],
    '#settings' => $element['#output']['settings']
  );

  // Load carousel settings from the instance id.
  $instance_settings = _owlcarousel_return_carousel_instance_settings($settings);

  // Provide legacy settings alter.
  drupal_alter('owlcarousel_settings', $instance_settings, $instance);

  // Element output /w attached.
  $element['#markup'] = owlsync_build($output);

  $element['#children'] = drupal_render($element['#markup']);
  $element['#attached'] = array(
    'js' => array(
      array(
        'data' => drupal_get_path('module', 'owlcarousel') .
          '/includes/js/owlcarousel.settings.js',
        'type' => 'file',
        'scope' => 'footer'
      ),
      array(
        'data' => array('owlcarousel' => array(
          $instance => array(
            'settings' => $instance_settings,
            'views' => array(
              'ajax_pagination' => variable_get('owlcarousel_override_ajax_pagination_' . $settings)
            ),
          ),
        ),),
        'type' => 'setting'
      )
    ),
    'library' => array(
      array(
        'owlcarousel',
        'owl-carousel'
      )
    ),
  );  // Provide alter before carousel is rendered.
  drupal_alter('owlcarousel_pre_render', $element);


  return $element;
}

/**
 * Build final output array.
 */
function owlsync_build($output) {
  return array(
    '#output' => $output,
    '#theme_wrappers' => array('owlsync_wrapper'),
  );
}